<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function test(Request $request)
    {
        if ($request->has('teleid')) {
            $telegramUserId = (int) $request->teleid;
        }else{
            $telegramUserId = 1;
        }
        if ($request->has('email')) {
            $email = $request->email;
        }else{
            $email = 'fulanodetal@gmail.com';
        }
        // $result = $this->hasUserAnEmail($telegramUserId);
        // dd($result);exit;
        $result = $this->setUserIdToEmail($telegramUserId,$email);
        dd($result);exit;
    }



    protected function hasUserAnEmail(int $telegramUserId) : boolean
    {
        $results = DB::select("SELECT COUNT(email) FROM users WHERE telegram_user_id=".$telegramUserId);
        
        return $results[0];
    }


    protected function setUserIdToEmail(int $telegramUserId, string $email) : int
    {
        $numAffected = DB::update("UPDATE users SET telegram_user_id = IF(telegram_user_id IS NULL,?,telegram_user_id) WHERE email= ?", [$telegramUserId, $email]);
        
        return $numAffected;
    }
}
