<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class KiwifyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handleWebhook(Request $request)
    {
        $this->validate($request, [
            'signature' => 'required',
            'Customer.email' => 'required|email',
            'Product.product_id' => 'required|string',
            'order_status' => 'required|in:paid,refunded,chargeback'
        ]);

        // receive order's data
        $payload = @file_get_contents('php://input');
        $order = json_decode($payload, true);

        $calculatedSignature = hash_hmac('sha1', json_encode($order), env("KIWIFY_TOKEN"));
        if($request->signature !== $calculatedSignature){
            return response()->json([
                'error' => (env('APP_ENV') == 'local' || env('APP_ENV') == 'dev') ? "Signature: " .$calculatedSignature : "signature does't match."
            ], 401);
        }
        
        //checa se já existe um usuário com mesmo e-mail para esse produto na kiwify
        $results_users = DB::select("SELECT * FROM purchase WHERE email = ? and product_id = ? and platform = 'kiwify'", [
            $request->Customer["email"],
            $request->Product["product_id"]
        ]);

        //se já existe
        if (count($results_users) > 0 ) {
            //update user
            DB::table('purchase')->where([
                ['platform','=','kiwify'],
                ['product_id','=',$request->Product["product_id"]],
                ['email','=',$request->Customer["email"]]
            ])->update(['order_status' => $request->order_status]);

            //se new order_status equals refund or chargeback, needs to remove user
            foreach($results_users as $user){
                $tele_user_id = (int) $user->telegram_user_id;
                if ($tele_user_id == 0 || is_null($tele_user_id)) {
                    Log::info('[kiwify][KickMember] Telegram_User_Id não encontrado. Usuário não removido.', ['email' => $user->email, 'tele_user_id' => $user->telegram_user_id]);
                    continue;
                }

                //kickmember here... options: request to a new route and let telegramcontroller handle | load lib here and do the things
            }

        }else{
            //insert user - platform field defaults to kiwify
            DB::insert('insert into purchase (email, product_id,order_status) values (?, ?,?)', 
                [$request->Customer["email"], $request->Product["product_id"], $request->order_status]
            );
        }
        
    }
}

