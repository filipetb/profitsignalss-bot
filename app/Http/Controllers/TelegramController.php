<?php

namespace App\Http\Controllers;

use Log;
use Illuminate\Http\Request;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Exception\TelegramException;

class TelegramController extends Controller
{

    protected $botToken;
    protected $botUserName;

    protected $botHookUrl;
    protected $botCertPem;

    protected $commandsPaths;
    protected $mysqlCon;

    public function __construct()
    {
        $this->botToken = env('TELEGRAM_BOT_TOKEN');
        $this->botUserName = env('TELEGRAM_BOT_USERNAME');
        $this->botHookUrl = route('internal-webhooktelebot-name');
        $this->botCertPem = env('TELEGRAM_BOT_CERTPEM');

        $this->commandsPaths = [
            __DIR__ . '/../../TelegramCommands',
        ];
        
        $this->mysqlCon = [
            'host'     => env('DB_HOST'),
            'user'     => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'database' => 'telegram_bot_lib',
        ];
    }

    /**
     * Handles Telegram PostBacks.
     *
     * @param  Request $request
     * @return Response
     */
    public function webhook(Request $request)
    {

        try {
            // Create Telegram API object
            $telegram = new Telegram($this->botToken, $this->botUserName);
        
            $telegram->addCommandsPaths($this->commandsPaths);

            $telegram->enableMySql($this->mysqlCon);

            // Handle telegram webhook request
            $telegram->handle();

        } catch (TelegramException $e) {
            
            Log::error($e->getMessage());
        }
    }

    /**
     * Set Telegram webhook.
     *
     * @param  Request $request
     * @return Response
     */
    public function setWebhook(Request $request)
    {

        try {
            // Create Telegram API object
            $telegram = new Telegram($this->botToken, $this->botUserName);
        
            if(strpos($this->botHookUrl,'https') !== 0){
                $this->botHookUrl = substr_replace($this->botHookUrl, 'https', 0, 4);
            }

            // Set webhook
            $result = $telegram->setWebHook($this->botHookUrl, array('certificate' => $this->botCertPem));
            if ($result->isOk()) {
                echo $result->getDescription();
            }

        } catch (TelegramException $e) {
            
            Log::error($e->getMessage());
        }
    }


    public function kickMember(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'product_id' => 'required|numeric'
        ]);

        try {
            // Create Telegram API object
            $telegram = new Telegram($this->botToken, $this->botUserName);
        
            $telegram->addCommandsPaths($this->commandsPaths);

            $teleCommandList = $telegram->getCommandsList();
            if ( isset($teleCommandList['kickmemberrr']) ) {
                $teleCommandList['kickmemberrr']->execute();
            }else{
                Log::error('Could not find Command kickmemberrr.', ['CommandList' => $teleCommandList]);
            }

        } catch (TelegramException $e) {
            
            Log::error($e->getMessage());
        }
    }

    public function kickExpiredMembers(Request $request)
    {
        try {
            // Create Telegram API object
            $telegram = new Telegram($this->botToken, $this->botUserName);
        
            $telegram->addCommandsPaths($this->commandsPaths);

            $teleCommandList = $telegram->getCommandsList();
            if ( isset($teleCommandList['kickexpiredmembers']) ) {
                $teleCommandList['kickexpiredmembers']->execute();
            }else{
                Log::error('Could not find Command kickexpiredmembersCommand.', ['CommandList' => $teleCommandList]);
            }

        } catch (TelegramException $e) {
            
            Log::error($e->getMessage());
        }
    }
}