<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
use Illuminate\Support\Facades\DB;

class EnterprofitsignalsvipCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'enterprofitsignalsvip';

    /**
     * @var string
     */
    protected $description = 'Invite user to Profit Signals Vip Channel';

    /**
     * @var string
     */
    protected $usage = '/enterprofitsignalsvip';

    /**
     * @var string
     */
    protected $version = '0.2.0';

    /**
     * @var bool
     */
    protected $private_only = true;



    public function execute() : ServerResponse
    {
        $message = $this->getMessage();

        $chat    = $message->getChat();
        $user    = $message->getFrom();
        $text    = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();
        $user_fn = $user->getFirstName();

        // Preparing response
        $data = [
            'chat_id'      => $chat_id,
            // Remove any keyboard by default
            'reply_markup' => Keyboard::remove(),
        ];

        if($this->hasTelegramUserProductAccess($user_id,2)){

            $profitsignalsChatId = '-1001358492002';

            //desbanir o cara, caso esteja
            Request::unbanChatMember([
                'chat_id' => $profitsignalsChatId,
                'user_id' => $user_id,
                'only_if_banned' => true
            ]);
            
            //invitar o cara
            $data2 = [
                'chat_id' => $profitsignalsChatId
            ];

            $serverResponseChat =  Request::getChat($data2);

            $needNewInvitationLink = true;

            if ($serverResponseChat->getOk()) {
                $chatChannel    = $serverResponseChat->getResult();
                $curIntiteLink = $chatChannel->getInviteLink();
                if (strpos($curIntiteLink,'https://t.me/joinchat') === 0) {
                    $data['text'] = 'Aqui o seu link exclusivo de acesso:'  . PHP_EOL .  $curIntiteLink;
                    $needNewInvitationLink = false;
                }
            }

            if($needNewInvitationLink){

                $serverResponseInvitation = Request::exportChatInviteLink($data2);
    
                if ($serverResponseInvitation->getOk()) {
    
                   $invitationLink = $serverResponseInvitation->getResult();
                   $data['text'] = 'Aqui o seu link exclusivo de acesso:'  . PHP_EOL .  $invitationLink;
    
                }else{
                    $data['text'] = 'Eita! 😳'  . PHP_EOL . 'Parece tivemos um problema ao gerar o seu link de acesso, por favor, contate nosso suporte.';
                    Log::error('[ProfitSignalsVIP] Falha ao Gerar Link de Convite.', ['ErrorCode:' => $serverResponseInvitation->getErrorCode(), 'ErrorCode:' => $serverResponseInvitation->getDescription()]);
                }

            }
        }else{
            $data['text'] = '⚠️ Ops...'  . PHP_EOL . 
                '<b>Parece que você ainda não tem uma assinatura ativa deste produto.</b>' . PHP_EOL . PHP_EOL . 
                'Considere adquirir este produto para obter acesso ao canal.' . PHP_EOL . PHP_EOL . 
                '<i>Se você acredita que tem uma assinatura válida e essa mensagem é um engano, por favor, entre em contato com nosso suporte.</i>';
            $data['parse_mode'] = 'html';
        }

        return Request::sendMessage($data);        // Send message!
    }


    protected function hasTelegramUserProductAccess(int $telegramUserId, int $product) : int
    {
        $ids = DB::select("SELECT id FROM users WHERE telegram_user_id= ?", [$telegramUserId]);
        
        if ($ids[0]->id) {
            $results = DB::select("SELECT COUNT(*) as num FROM subscriptions WHERE user_id= ? AND expires_at >= CURRENT_DATE AND product_id= ?", [$ids[0]->id, $product]);
        
            return $results[0]->num;
        }else{
            return 0;
        }

        
    }

}