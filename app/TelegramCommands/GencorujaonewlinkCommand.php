<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GencorujaonewlinkCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'gencorujaonewlink';

    /**
     * @var string
     */
    protected $description = 'Generate New Invitation Links';

    /**
     * @var string
     */
    protected $usage = '/gencorujaonewlink';

    /**
     * @var string
     */
    protected $version = '0.1.0';

    /**
     * @var bool
     */
    protected $private_only = true;



    public function execute() : ServerResponse
    {
        $message = $this->getMessage();

        $chat    = $message->getChat();
        $user    = $message->getFrom();
        $text    = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();
        $user_fn = $user->getFirstName();

        // Preparing response
        $data = [
            'chat_id'      => $chat_id,
            // Remove any keyboard by default
            'reply_markup' => Keyboard::remove(),
        ];


        $data2 = [
            'chat_id' => '-1001427831615'
        ];

            
        $serverResponseInvitation = Request::exportChatInviteLink($data2);

        if ($serverResponseInvitation->getOk()) {

            $invitationLink = $serverResponseInvitation->getResult();
            $data['text'] = 'Novo link exclusivo de acesso:'  . PHP_EOL .  $invitationLink;

        }else{
            $data['text'] = 'Eita! 😳'  . PHP_EOL . 'Parece tivemos um problema ao gerar um novo link de acesso, por favor, contate nosso suporte.';
            Log::error('[ProfitSignalsVIP] Falha ao Gerar novo Link de Convite.', ['ErrorCode:' => $serverResponseInvitation->getErrorCode(), 'ErrorCode:' => $serverResponseInvitation->getDescription()]);
        }
        

        return Request::sendMessage($data);        // Send message!
    }

}