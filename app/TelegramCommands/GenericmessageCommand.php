<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GenericmessageCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'genericmessage';

    /**
     * @var string
     */
    protected $description = 'Handle generic message';

    /**
     * @var string
     */
    protected $version = '1.1.1';
    
    /**
     * @var bool
    */
    protected $need_mysql = true;

    /**
     * Command execute method if MySQL is required but not available
     *
     * @return ServerResponse
    */
    public function executeNoDb(): ServerResponse
    {
        // Do nothing
        return Request::emptyResponse();
    }

    /**
     * Main command execution
     *
     * @return ServerResponse
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();

        if (is_null($message)) {
            return Request::emptyResponse();
        }

        $chat    = $message->getChat();
        $user    = $message->getFrom();
        $text    = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();
        $user_fn = $user->getFirstName();

        // Preparing response
        $data = [
            'chat_id'      => $chat_id,
            // Remove any keyboard by default
            'reply_markup' => Keyboard::remove(),
        ];

        Log::debug($text, ['First_Name' => $user_fn, 'user_id' => $user_id, 'chat_id' => $chat_id]);

        if (in_array($text,['-Acessar Canal Corujao VIP','-Acessar Canal Profit Signals VIP'])) {
            
            list($numEmail, $email) = $this->hasUserAnEmail((int) $user_id);

            if ($numEmail > 0) {
                //verificar se e-mail tem uma assinatura válida pro produto que ele quer acessar
                    //se tiver, retorna convite do grupo
                if ($text == '-Acessar Canal Corujao VIP') {
                    return $this->telegram->executeCommand('entercorujaovip');
                }else{
                    return $this->telegram->executeCommand('enterprofitsignalsvip');
                }
            }else{
                
                return $this->telegram->executeCommand('setemail');

            }

        }else{

            // If a conversation is busy, execute the conversation command after handling the message.
            $conversation = new Conversation(
                $message->getFrom()->getId(),
                $message->getChat()->getId()
            );

            // Fetch conversation command if it exists and execute it.
            if ($conversation->exists() && $command = $conversation->getCommand()) {

                return $this->telegram->executeCommand($command);

            }else{

                $data['text'] = "Fala {$user_fn}, tudo certo? Caso precise de algo, só digitar /menu";
                return Request::sendMessage($data);

            }

        }

    }

    protected function hasUserAnEmail(int $telegramUserId) : array
    {
        $results = DB::select("SELECT COUNT(email) as num, email FROM users WHERE telegram_user_id=".$telegramUserId);
        
        return [$results[0]->num, $results[0]->email];
    }
}