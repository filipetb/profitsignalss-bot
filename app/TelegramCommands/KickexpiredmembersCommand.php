<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ServerResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class KickexpiredmembersCommand extends UserCommand
{
    protected $name = 'kickexpiredmembersCommand';                  
    protected $description = 'Kick all expired member';       
    protected $usage = '/kickexpiredmembersCommand';                
    protected $version = '1.0.0';                  


    public function execute() : ServerResponse
    {

        $users_to_ban = $this->getTelegramUsersIdToBan();

        foreach ($users_to_ban as $user) {

            if ($user->product_id == 1) { //corujao
                $data = [
                    'chat_id' => '-1001427831615'
                ];
            }elseif($user->product_id == 2){ //profit signals
                $data = [
                    'chat_id' => '-1001358492002'
                ];
            }else{
                continue;
            }

            $tele_user_id = (int) $user->telegram_user_id;

            if ($tele_user_id == 0 || is_null($tele_user_id)) {
                Log::info('[KickMember] Id do membro Inválida. Pulado.', ['email' => $user->email, 'tele_user_id' => $user->telegram_user_id]);
                continue;
            }

            $data['user_id'] = $tele_user_id;

            $serverResponseChat =  Request::kickChatMember($data);

            if ($serverResponseChat->getOk()) {
                if ($user->product_id == 1) {
                    Log::info('[KickMember] [CORUJAO] Banido com sucesso.', ['email' => $user->email, 'tele_user_id' => $user->telegram_user_id]);
                }else{
                    Log::info('[KickMember] [PROFITSIGNALS] Banido com sucesso.', ['email' => $user->email, 'tele_user_id' => $user->telegram_user_id]);
                }
                
            }else{
                //log error
                if ($user->product_id == 1) {
                    Log::info('[KickMember] [CORUJAO] Falha Kickar Membro.', ['email' => $user->email, 'tele_user_id' => $user->telegram_user_id, 'ErrorCode:' => $serverResponseChat->getErrorCode(), 'ErrorCode:' => $serverResponseChat->getDescription()]);
                }else{
                    Log::info('[KickMember] [PROFITSIGNALS] Falha Kickar Membro.', ['email' => $user->email, 'tele_user_id' => $user->telegram_user_id, 'ErrorCode:' => $serverResponseChat->getErrorCode(), 'ErrorCode:' => $serverResponseChat->getDescription()]);
                }
            }

            sleep(1);

        }


        return Request::emptyResponse();
    }

    protected function getTelegramUsersIdToBan()
    {
        $users_to_ban = DB::select("SELECT `subscriptions`.product_id, `users`.telegram_user_id, `users`.email FROM `subscriptions` JOIN `users` on `users`.id = `subscriptions`.user_id where subscriptions.expires_at < CURRENT_DATE AND `users`.id NOT IN (SELECT `subscriptions`.user_id FROM  `subscriptions` WHERE `subscriptions`.`expires_at` >= CURRENT_DATE)");
        return $users_to_ban;
    }
}