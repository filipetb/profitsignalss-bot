<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ServerResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class KickmemberrrCommand extends UserCommand
{
    protected $name = 'kickmemberrr';                  
    protected $description = 'Kick a member';       
    protected $usage = '/kickmemberrr';                
    protected $version = '1.0.0';                  


    public function execute() : ServerResponse
    {
        if ( !isset($_GET['email']) || !isset($_GET['product_id']) ) {
            return Request::emptyResponse();
        }

        if ($_GET['product_id'] == 1) { //corujao
            $data = [
                'chat_id' => '-1001427831615'
            ];
        }elseif($_GET['product_id'] == 2){ //profit signals
            $data = [
                'chat_id' => '-1001358492002'
            ];
        }else{
            return Request::emptyResponse();
        }


        $tele_user_id = $this->getTelegramUserIdByEmail($_GET['email']);

        if ($tele_user_id == 0) {
            //log error
            Log::error('[KickMember] Falha ao encontrar membro.', ['email' => $_GET['email'], 'tele_user_id' => $tele_user_id]);
            return Request::emptyResponse();
        }

        $data['user_id'] = $tele_user_id;
        
        $serverResponseChat =  Request::kickChatMember($data);

        if ($serverResponseChat->getOk()) {
            Log::info('[KickMember] Banido com sucesso.', ['email' => $_GET['email'], 'tele_user_id' => $tele_user_id]);
        }else{
            //log error
            Log::error('[KickMember] Falha Kickar Membro.', ['email' => $_GET['email'], 'tele_user_id' => $tele_user_id, 'ErrorCode:' => $serverResponseChat->getErrorCode(), 'ErrorCode:' => $serverResponseChat->getDescription()]);
        }

        return Request::emptyResponse();
    }

    protected function getTelegramUserIdByEmail(string $email) : int
    {
        $tele_ids = DB::select("SELECT telegram_user_id FROM users WHERE email= ?", [$email]);
        
        if (count($tele_ids) > 0 ) {
            return (int) $tele_ids[0]->telegram_user_id;
        }else{
            return 0;
        }
    }
}