<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Exception\TelegramException;

class MenuCommand extends UserCommand
{
    protected $name = 'menu';                      
    protected $description = 'Show bot option'; 
    protected $usage = '/menu';                    
    protected $version = '1.0.0';                 


    public function execute() : ServerResponse
    {
        // /** @var Keyboard[] $keyboards */
        // $keyboards = [];

        // // Simple digits
        // $keyboards[] = new Keyboard(
        //     ['7', '8', '9'],
        //     ['4', '5', '6'],
        //     ['1', '2', '3'],
        //     [' ', '0', ' ']
        // );

        // // Digits with operations
        // $keyboards[] = new Keyboard(
        //     ['7', '8', '9', '+'],
        //     ['4', '5', '6', '-'],
        //     ['1', '2', '3', '*'],
        //     [' ', '0', ' ', '/']
        // );

        // // Short version with 1 button per row
        // $keyboards[] = new Keyboard('A', 'B', 'C');

        // // Some different ways of creating rows and buttons
        // $keyboards[] = new Keyboard(
        //     ['text' => 'A'],
        //     'B',
        //     ['C', 'D']
        // );

        // // Buttons to perform Contact or Location sharing
        // $keyboards[] = new Keyboard([
        //     ['text' => 'Send my contact', 'request_contact' => true],
        //     ['text' => 'Send my location', 'request_location' => true],
        // ]);

        // // Shuffle our example keyboards and return a random one
        // shuffle($keyboards);
        // $keyboard = end($keyboards)
        //     ->setResizeKeyboard(true)
        //     ->setOneTimeKeyboard(true)
        //     ->setSelective(false);

        $keyboard = new Keyboard('-Acessar Canal Corujao VIP', '-Acessar Canal Profit Signals VIP');
        $keyboard->setResizeKeyboard(true)->setOneTimeKeyboard(true)->setSelective(false);

        return $this->replyToChat('Aperte em um botão!', [
            'reply_markup' => $keyboard,
        ]);
    }

}