<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Exception\TelegramException;
use Illuminate\Support\Facades\DB;

class SetemailCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'setemail';

    /**
     * @var string
     */
    protected $description = 'Relates a given e-mail for current user';

    /**
     * @var string
     */
    protected $usage = '/setemail';

    /**
     * @var string
     */
    protected $version = '0.1.1';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * @var bool
     */
    protected $private_only = true;

    /**
     * Conversation Object
     *
     * @var Conversation
     */
    protected $conversation;



    public function execute(): ServerResponse
    {
        $message = $this->getMessage();

        $chat    = $message->getChat();
        $user    = $message->getFrom();
        $text    = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();
        $user_fn = $user->getFirstName();

        // Preparing response
        $data = [
            'chat_id'      => $chat_id,
            // Remove any keyboard by default
            'reply_markup' => Keyboard::remove(),
        ];


        // Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());

        // Load any existing notes from this conversation
        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];

        // Load the current state of the conversation
        $state = $notes['state'] ?? 0;

        $result = Request::emptyResponse();
        
        // State machine
        // Every time a step is achieved the state is updated
        switch ($state) {
            case 0:
                $data['text'] = 'Certo...' . PHP_EOL . PHP_EOL . 
                    'Antes disso, preciso que <b>me informe seu e-mail</b> para vincularmos com sua conta do Telegram.' . PHP_EOL . PHP_EOL . 
                    'PS: Digite o mesmo e-mail que usou para comprar este produto.';
                $data['parse_mode'] = 'html';

                $result = Request::sendMessage($data);

                $notes['state'] = 1;
                $this->conversation->update();
                break;
            case 1:
                if (!filter_var($text, FILTER_VALIDATE_EMAIL)) {
                    $data['text'] = 'Hey!'  . PHP_EOL . 'Preciso que me informe um e-mail válido!'  . PHP_EOL . 'Tente novamente 😅';
                }else{
                    //atribuir o user_id dele ao e-mail digitado UPDATE users SET telegram_user_id=$user_id WHERE e-mail=$e_mail_digitado
                    if($this->setUserIdToEmail((int) $user_id, $text) < 1){
                        $data['text'] = 'Hmm...'  . PHP_EOL . 
                            'Parece que esse e-mail não está disponível em nosso sistema.' . PHP_EOL . PHP_EOL . 
                            '<i>Se você acredita que digitou o e-mail correto, por favor, entre em contato com nosso suporte.</i>';
                            $data['parse_mode'] = 'html';
                    }else{
                        $data['text'] = 'Telegram vinculado com sucesso! 😎' . PHP_EOL . PHP_EOL .  
                            'Digite /menu e escolha a opção desejada.';
                    }
                    unset($notes['state']);
                    $this->conversation->stop();
                }
                $result = Request::sendMessage($data);
                break;
        }

        return $result;
    }


    protected function setUserIdToEmail(int $telegramUserId, string $email) : int
    {
        $numAffected = DB::update("UPDATE users SET telegram_user_id = IF(telegram_user_id IS NULL,?,telegram_user_id) WHERE email= ?", [$telegramUserId, $email]);
        
        return $numAffected;
    }

}