<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ServerResponse;

class StartCommand extends UserCommand
{
    protected $name = 'start';                      // Your command's name
    protected $description = 'Inicializa o Bot';    // Your command description
    protected $usage = '/start';                    // Usage of your command
    protected $version = '1.2.1';                   // Version of your command


    public function execute() : ServerResponse
    {
        $message = $this->getMessage();            // Get Message object

        $chat_id = $message->getChat()->getId();   // Get the current Chat ID

        //$user_id = $message->getFrom()->getId(); // Get the current User ID

        $data = [                                  // Set up the new message data
            'chat_id' => $chat_id,                 // Set Chat ID to send the message to
            'text'    => 'Que massa tê-lo aqui! 🤩' . PHP_EOL .'Digite /menu para visualizar as opções disponíveis.', // Set message to send
        ];

        return Request::sendMessage($data);        // Send message!
    }

}