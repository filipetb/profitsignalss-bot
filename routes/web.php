<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return response()->json([
        'Env' => env('APP_ENV'),
        'Version' => $router->app->version()
    ]);
});

$router->get('/test','ExampleController@test');

$router->post('/webhook/telegram/tkxwp1fo', ['as' => 'internal-webhooktelebot-name', 'uses' => 'TelegramController@webhook']);

$router->group(['prefix' => '/webhook/telegram'], function () use ($router) {
    
    $router->get('set/xf87n2ak', 'TelegramController@setWebhook'); 
    
    $router->get('kick/expired', 'TelegramController@kickExpiredMembers');  

    $router->group(['prefix' => 'monetizze'], function() use ($router) {
        $router->get('kick', 'MonetizzeController@kickMember');
    });

    $router->group(['prefix' => 'kiwify'], function() use ($router) {
        //Handle all types os request came from kiwify webhook registered
        $router->post('handle', 'KiwifyController@handleWebhook');
    });
});